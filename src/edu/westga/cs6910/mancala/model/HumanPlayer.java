package edu.westga.cs6910.mancala.model;

/**
 * HumanPlayer represents a human player in the game Mancala.
 * 
 * @author CS6910
 * @version Summer 2016
 * @author Chris Jones
 * @version June 11, 2019
 */
public class HumanPlayer extends AbstractPlayer {
	private Game theGame;

	/**
	 * Creates a new ComputerPlayer with the specified name.
	 * 
	 * @param name    this Player's name
	 * @param theGame The Game that this player represents
	 * 
	 * @requires name != null
	 * @ensure name().equals(name) && getTotal() == 0
	 */
	public HumanPlayer(String name, Game theGame) {
		super(name, theGame);
		this.theGame = theGame;
	}

	/**
	 * This method has the human take his or her turn. 
	 */
	@Override
	public void takeTurn(int pitChoice) {
		this.captureStones(pitChoice);
		this.theGame.distributeStonesFrom(pitChoice);
		
		super.setIsMyTurn(false);
	}

	@Override
	public void captureStones(int pitChoice) {
		this.theGame.setAreStonesCaptured(false);
		int oppositePit = 0;
		int lastPitOfTurn = 0;
		if (this.theGame.getGameBoard()[pitChoice] + pitChoice <= this.theGame.getBoardSize() - 1) {
			lastPitOfTurn = this.theGame.getGameBoard()[pitChoice] + pitChoice;
		} else {
			lastPitOfTurn = (this.theGame.getStones(pitChoice)) - 1;
		}

		if (lastPitOfTurn != this.theGame.getBoardSize() - 1
				&& lastPitOfTurn != ((this.theGame.getBoardSize() - 2) / 2)) {
			oppositePit = (this.theGame.getBoardSize() - 2) - lastPitOfTurn;
		}
		
		if (lastPitOfTurn < ((this.theGame.getBoardSize() - 2) / 2) && this.theGame.getStones(lastPitOfTurn) == 0 && this.theGame.getStones(oppositePit) > 0
				&& this.theGame.getCurrentPlayer() == this.theGame.getHumanPlayer()) {
			this.theGame.getGameBoard()[(this.theGame.getBoardSize() - 2) / 2] = this.theGame.getStones((this.theGame.getBoardSize() - 2) / 2)
					+ this.theGame.getStones(oppositePit);
			
			this.theGame.setPitStones((this.theGame.getBoardSize() - 2) / 2, this.theGame.getStones((this.theGame.getBoardSize() - 2) / 2) + this.theGame.getStones(oppositePit));
			this.theGame.setPitStones(oppositePit, 0);
			this.theGame.setAreStonesCaptured(true);
		}
		
	}

}
