package edu.westga.cs6910.mancala.model.strategies;

/**
 * This class will detail the far strategy for the computer player
 * @author Chris Jones
 * @version June 23, 2019
 *
 */
public class FarStrategy implements SelectStrategy {
	
	/**
	 * This method prepares the choice that will facilitate the computer choosing the farthest option away
	 * that has a pit
	 * @param gameBoard of game 
	 * @return chosen pit
	 */
	@Override
	public int selectPit(int[] gameBoard) {
		int pitChoice = gameBoard.length - 4;
		while (gameBoard[pitChoice] == 0 && pitChoice < gameBoard.length) {
			pitChoice++;
		}
		return pitChoice;
	}
}
