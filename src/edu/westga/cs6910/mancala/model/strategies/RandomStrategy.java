package edu.westga.cs6910.mancala.model.strategies;

/**
 * This class sets a random chance of the near strategies and far strategies
 * being chosen
 * 
 * @author Chris Jones
 * @version June 23, 2019
 *
 */
public class RandomStrategy implements SelectStrategy {

	/**
	 * This method returns a random chance of a far strategy or a near strategy
	 * 
	 * @param gameBoard of game to be played
	 * @return pitChoice number of pit to be chosen
	 */
	public int selectPit(int[] gameBoard) {
		int pitChoice = 0;
		double randomChance = Math.random();
		if (randomChance <= 0.33) {
			pitChoice = gameBoard.length - 2;
		} else if (randomChance > 0.33 && randomChance < 0.66) {
			pitChoice = gameBoard.length - 3;
		} else if (randomChance >= 0.66) {
			pitChoice = gameBoard.length - 4;
		}

		while (gameBoard[pitChoice] == 0) {
			pitChoice = this.randomPitEmpty(pitChoice, gameBoard);
		}

		return pitChoice;
	}

	private int randomPitEmpty(int pitChoice, int[] gameBoard) {
		double randomNumber = Math.random();
		if (pitChoice == gameBoard.length - 4 && randomNumber > 0.5) {
			pitChoice = gameBoard.length - 3;
		} else if (pitChoice == gameBoard.length - 4 && randomNumber <= 0.5) {
			pitChoice = gameBoard.length - 2;
		} else if (pitChoice == gameBoard.length - 2 && randomNumber <= 0.5) {
			pitChoice = gameBoard.length - 4;
		} else if (pitChoice == gameBoard.length - 2 && randomNumber > 0.5) {
			pitChoice = gameBoard.length - 3;
		} else if (pitChoice == gameBoard.length - 3 && randomNumber > 0.5) {
			pitChoice++;
		} else if (pitChoice == gameBoard.length - 3 && randomNumber <= 0.5) {
			pitChoice--;
		}
		return pitChoice;
	}
}
