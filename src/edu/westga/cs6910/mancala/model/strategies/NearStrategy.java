package edu.westga.cs6910.mancala.model.strategies;

/**
 * This class captures the near strategy for the Computer Player
 * @author Chris Jones
 * @version June 23, 2019
 *
 */
public class NearStrategy implements SelectStrategy {
	
	@Override
	public int selectPit(int[] gameBoard) {
		int pitChoice = gameBoard.length - 2;
		while (gameBoard[pitChoice] == 0 && pitChoice < gameBoard.length) {
			pitChoice--;
		}
		return pitChoice;
	}

}
