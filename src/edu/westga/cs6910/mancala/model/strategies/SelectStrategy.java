
package edu.westga.cs6910.mancala.model.strategies;

/**
 * This defines the interface for all game-play options
 * @author Chris Jones
 * @version June 23, 2019
 *
 */
public interface SelectStrategy {
	
	/**
	 * This method accepts the gameboard for the strategy and returns the pit number selected
	 * @param gameBoard chosen
	 * @return pit number selected
	 * 
	 */
	int selectPit(int[] gameBoard);

}
