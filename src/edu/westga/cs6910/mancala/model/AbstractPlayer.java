package edu.westga.cs6910.mancala.model;


/**
 * This abstract class is the superclass for HumanPlayer and ComputerPlayer. It creates and keeps track
 * of information for a generic Player object
 * @author Chris Jones
 * @version June 11, 2019
 *
 */
public abstract class AbstractPlayer implements Player {
	private boolean isMyTurn;
	private String name;
	
	
	/**
	 * This abstract class creates and keeps track of information for different types of
	 * player objects
	 * @param name name of player
	 * @param theGame game player is playing
	 */
	public 	AbstractPlayer(String name, Game theGame) {
		if (name == null) {
			throw new IllegalArgumentException("Invalid name");
		}
		
		if (theGame == null) {
			throw new IllegalArgumentException("Invalid Game object");
		}
		this.name = name;
		this.isMyTurn = false;
	}
	
	@Override	
	/**
	 * @see Player#getIsMyTurn()
	 */
	public boolean getIsMyTurn() {
		return this.isMyTurn;
	}
	
	@Override
	/**
	 * @see Player#getName()
	 */
	public String getName() {
		return this.name;
	}	
	
	@Override
	/**
	 * @see Player#setIsMyTurn
	 */
	public void setIsMyTurn(boolean isMyTurn) {
		this.isMyTurn = isMyTurn;
	}
	
	@Override
	/**
	 * @see Player#takeTurn
	 * 
	 */
	public abstract void takeTurn(int pitChoice);
	
	@Override
	/**
	 * see Player#captureStones
	 * 
	 */
	public abstract void captureStones(int pitChoice);

}
	

