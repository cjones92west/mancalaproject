package edu.westga.cs6910.mancala.model;

import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

/**
 * Game represents a Mancala game.
 * 
 * @author CS6910
 * @version Summer 2019
 * @author Chris Jones
 * @version June 11, 2019
 */
public class Game implements Observable {
	private int[] theBoard;

	private ObjectProperty<Player> currentPlayerObject;
	private HumanPlayer theHuman;
	private ComputerPlayer theComputer;
	private Player firstPlayer;
	private Player theWinner;
	private boolean isGameOver;
	private boolean areStonesCaptured;
	private boolean getAnotherTurn;

	/**
	 * Creates a Mancala Game with the specified Players
	 * 
	 */
	public Game() {

		this.theHuman = new HumanPlayer("Human", this);
		this.theComputer = new ComputerPlayer(this);

		this.currentPlayerObject = new SimpleObjectProperty<Player>();

		this.theBoard = new int[8];
	}

	/**
	 * Initializes the game for play.
	 * 
	 * @param firstPlayer the Player who takes the first turn
	 * 
	 * @require firstPlayer != null &&
	 * 
	 * @ensures whoseTurn().equals(firstPlayer)
	 */
	public void startNewGame(Player firstPlayer) {
		this.resetBoard();
		this.currentPlayerObject.setValue(firstPlayer);
		this.setFirstPlayer(firstPlayer);
		firstPlayer.setIsMyTurn(true);
	}
	
	/**
	 * This method returns whether a player gets another turn
	 * @return boolean value anotherTurn
	 */
	public boolean getAnotherTurn() {
		return this.getAnotherTurn;
	}
	
	/**
	 * This method returns the winner of the game
	 * @return the winner
	 */
	public Player getTheWinner() {
		return this.theWinner;
	}
	
	/**
	 * This method returns the value of the areStonesCaptured boolean variable
	 * @return boolean value
	 */
	public boolean getAreStonesCaptured() {
		return this.areStonesCaptured;
	}
	
	/**
	 * This method sets whether the stones have been captured (for resetting purposes)
	 * @param areStonesCaptured boolean value
	 */
	public void setAreStonesCaptured(boolean areStonesCaptured) {
		this.areStonesCaptured = areStonesCaptured;
	}

	/**
	 * This method returns the first player of a game
	 * @return first player
	 */
	public Player getFirstPlayer() {
		return this.firstPlayer;
	}
		

	/**
	 * This method sets the first player of the game
	 * @param firstPlayer of game
	 * Precondition: firstPlayer != null
	 */
	public void setFirstPlayer(Player firstPlayer) {
		if (firstPlayer == null) {
			throw new IllegalArgumentException("There must be a true player");
		}
		this.firstPlayer = firstPlayer;
	}

	/**
	 * Distributes the stones located in pitNumber to all subsequent pits, one at a
	 * time in counter-clockwise order
	 * 
	 * @param pitNumber The pit number where the stones are to be taken
	 */
	public void distributeStonesFrom(int pitNumber) {
		if (pitNumber < 0) {
			throw new IllegalArgumentException("Pit number cannot be negative");
		}
		int thisPit = pitNumber + 1;
		int stonesInPit = this.theBoard[pitNumber];
		this.theBoard[pitNumber] = 0;
		while (stonesInPit > 0) {
			this.theBoard[(thisPit % this.theBoard.length)] += 1;
			thisPit++;
			stonesInPit--;
		}

	}

	/**
	 * Returns the number of slots (pits and stores) on the board
	 * 
	 * @return The number of slots on the board
	 */
	public int getBoardSize() {
		return this.theBoard.length;
	}

	/**
	 * Returns the computer Player object.
	 * 
	 * @return the computer Player
	 */
	public ComputerPlayer getComputerPlayer() {
		return this.theComputer;
	}

	/**
	 * Returns the Player whose turn it is.
	 * 
	 * @return the current Player
	 */
	public Player getCurrentPlayer() {
		return this.currentPlayerObject.getValue();
	}

	/**
	 * Returns the human Player object.
	 * 
	 * @return the human Player
	 */
	public HumanPlayer getHumanPlayer() {
		return this.theHuman;
	}

	/**
	 * Returns whether the game has completed yet or not
	 * 
	 * @return true if the game is over; false otherwise
	 */
	public boolean getIsGameOver() {
		return this.isGameOver;
	}

	/**
	 * Returns the number of stones in the specified pit
	 * 
	 * @param pitNumber The pit location that is potentially holding stones
	 * @return The number of stones in the specified pit
	 */
	public int getStones(int pitNumber) {
		if (pitNumber < 0) {
			throw new IllegalArgumentException("Pit number cannot be negative");
		}
		return this.theBoard[pitNumber];
	}

	/**
	 * Conducts a move in the game, allowing the appropriate Player to take a turn.
	 * Has no effect if the game is over.
	 * 
	 * @param pitChoice The pit number where the stones will be taken from
	 * 
	 * @requires !isGameOver()
	 * 
	 * @ensures !whoseTurn().equals(whoseTurn()@prev)
	 */
	public void play(int pitChoice) {
		Player currentPlayer = this.currentPlayerObject.getValue();
		this.getAnotherTurn = false;

		int lastPitOfTurn = 0;
		if (currentPlayer instanceof ComputerPlayer) {
			pitChoice = this.getComputerPlayer().getStrategy().selectPit(this.getGameBoard());
		}
		if (pitChoice != -1) {
			lastPitOfTurn = this.theBoard[pitChoice] + pitChoice;
		}
		
		currentPlayer.takeTurn(pitChoice);
		Player winner = this.getFinisher();
		if (winner != null) {
			this.finishGame(winner);
		} else if (lastPitOfTurn == this.getBoardSize() - 1 || lastPitOfTurn == ((this.getBoardSize() - 2)) / 2) {
			this.resetPlayerValues();
			this.getAnotherTurn = true;
		} else {
			this.swapWhoseTurn();
		}
	}

	private Player getFinisher() {
		int humanStoneCount = 0;
		for (int index = 0; index < this.theBoard.length / 2 - 1; index++) {
			humanStoneCount += this.theBoard[index];
		}
		if (humanStoneCount == 0) {
			return this.theHuman;
		}

		int computerStoneCount = 0;
		for (int index = this.theBoard.length / 2; index < this.theBoard.length - 1; index++) {
			computerStoneCount += this.theBoard[index];
		}
		if (computerStoneCount == 0) {
			return this.theComputer;
		}
		return null;
	}

	/**
	 * This method restarts the game 
	 */
	public void reStartGame() {
		this.isGameOver = false;
		this.theWinner = null;
		this.resetBoard();
		this.resetBanks();
		this.resetToFirstPlayer();
	
	}

	private void finishGame(Player finisher) {
		int humanStore = this.theBoard.length / 2 - 1;
		int computerStore = this.theBoard.length - 1;
		if (finisher.equals(this.theHuman)) {
			int storeIndex = humanStore;
			for (int index = this.theBoard.length / 2; index < this.theBoard.length - 1; index++) {
				this.theBoard[storeIndex] += this.theBoard[index];
				this.theBoard[index] = 0;
			}
		} else {
			int storeIndex = computerStore;
			for (int index = 0; index < this.theBoard.length / 2 - 1; index++) {
				this.theBoard[storeIndex] += this.theBoard[index];
				this.theBoard[index] = 0;
			}
		}
		
		if (this.theBoard[humanStore] > this.theBoard[computerStore]) {
			this.theWinner = this.theHuman;
		} else if (this.theBoard[computerStore] > this.theBoard[humanStore]) {
			this.theWinner = this.theComputer;
		}
		
		this.isGameOver = true;
		this.currentPlayerObject.setValue(null);
	}

	/**
	 * Returns a copy of the game board keeping track of the number of stones in
	 * each pit
	 * 
	 * @return The game board
	 */
	public int[] getGameBoard() {
		return this.theBoard.clone();
	}
	
	/**
	 * This method sets a pit with the number of stones should the need arise
	 * @param pitNumber number of pit to be adjusted
	 * @param numberOfStones new number of stones to be in pit
	 */
	public void setPitStones(int pitNumber, int numberOfStones) {
		this.theBoard[pitNumber] = numberOfStones;
		
	}

	/**
	 * Sets up the board such that there is exactly 1 stone in each pit
	 */
	public void resetBoard() {
		for (int index = 0; index < this.theBoard.length / 2 - 1; index++) {
			this.theBoard[index] = 1;
			this.theBoard[index + this.theBoard.length / 2] = 1;
		}
	}

	/**
	 * This method resets the banks when necessary
	 */
	private void resetBanks() {
		for (int index = 0; index < this.theBoard.length; index++) {
			if (index == this.theBoard.length / 2 - 1 || index == this.theBoard.length - 1) {
				this.theBoard[index] = 0;
			}
		}
	}

	/**
	 * This method switches the players after a turn has gone
	 */
	private void swapWhoseTurn() {
		if (this.currentPlayerObject.getValue() == this.theHuman) {
			this.currentPlayerObject.setValue(this.theComputer);
			this.theComputer.setIsMyTurn(true);
			this.theHuman.setIsMyTurn(false);
		} else {
			this.currentPlayerObject.setValue(this.theHuman);
			this.theHuman.setIsMyTurn(true);
			this.theComputer.setIsMyTurn(false);
		}
	}

	/**
	 * This method triggers the invalidation methods in order to reset values on the board so the
	 * current player can go again or start the game over
	 */
	private void resetPlayerValues() {
		if (this.currentPlayerObject.getValue() == this.theHuman) {
			this.currentPlayerObject.setValue(this.theComputer);
			this.currentPlayerObject.setValue(this.theHuman);
		} else {
			this.currentPlayerObject.setValue(this.theHuman);
			this.currentPlayerObject.setValue(this.theComputer);
		}

	}
	
	private void resetToFirstPlayer() {		
		this.resetPlayerValues();
		if (this.getFirstPlayer() == this.theHuman) {
			this.getHumanPlayer().setIsMyTurn(true);
			
		} else {
		
			this.getComputerPlayer().setIsMyTurn(true);
		}

	}
	

	/**
	 * Returns a String showing the current score, or, if the game is over, the name
	 * of the winner.
	 * 
	 * @return a String representation of this Game
	 */
	public String toString() {
		String result = this.theHuman.getName() + ": " + this.theBoard[this.theBoard.length / 2 - 1]
				+ System.getProperty("line.separator");
		result += this.theComputer.getName() + ": " + this.theBoard[this.theBoard.length - 1];
		if (this.isGameOver && this.theWinner != null) {
			result += System.getProperty("line.separator") + this.theWinner.getName() + " wins";
		} else if (this.isGameOver && this.theWinner == null) {
			result += "Tie game";
		}

		return result;
	}

	@Override
	public void addListener(InvalidationListener theListener) {
		this.currentPlayerObject.addListener(theListener);

	}

	@Override
	public void removeListener(InvalidationListener theListener) {
		this.currentPlayerObject.removeListener(theListener);
	}

}
