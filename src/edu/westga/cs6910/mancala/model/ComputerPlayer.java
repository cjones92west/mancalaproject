package edu.westga.cs6910.mancala.model;

import edu.westga.cs6910.mancala.model.strategies.SelectStrategy;

/**
 * ComputerPlayer represents a very simple automated player in the game Mancala.
 * It always selects the closest pit for stones to be distributed
 * 
 * @author CS6910
 * @version Summer 2016
 * @author Chris Jones
 * @version June 11, 2019
 */
public class ComputerPlayer extends AbstractPlayer {
	private static final String NAME = "Simple computer";
	private static SelectStrategy chosenStrategy;

	private Game theGame;

	/**
	 * Creates a new ComputerPlayer with the specified name.
	 * 
	 * @param theGame The Game that this player represents
	 * 
	 */
	public ComputerPlayer(Game theGame) {
		super(NAME, theGame);
		this.theGame = theGame;

	}

	/**
	 * This method sets the strategy the computer player will use
	 * 
	 * @param theStrategy chosen strategy for computer to use
	 */
	public void setStrategy(SelectStrategy theStrategy) {
		ComputerPlayer.chosenStrategy = theStrategy;
	}

	/**
	 * This method gets the strategy selected for the computer
	 * 
	 * @return strategy
	 */
	public SelectStrategy getStrategy() {
		return ComputerPlayer.chosenStrategy;
	}

	/**
	 * This method handles what happens when a randomly chosen pit is empty
	 * 
	 * @param pitChoice
	 * @return pit chosen at random
	 */

	/**
	 * This method has the computer take its turn
	 */
	@Override
	public void takeTurn(int pitChoice) {
		this.captureStones(pitChoice);
		this.theGame.distributeStonesFrom(pitChoice);

		super.setIsMyTurn(false);

	}

	@Override
	public void captureStones(int pitChoice) {
		this.theGame.setAreStonesCaptured(false);
		int [] gameBoard = this.theGame.getGameBoard();
		int oppositePit = 0;
		int lastPitOfTurn = 0;
		if (gameBoard[pitChoice] + pitChoice <= gameBoard.length - 1) {
			lastPitOfTurn = gameBoard[pitChoice] + pitChoice;
		} else {
			lastPitOfTurn = (this.theGame.getStones(pitChoice)) - 1;
		}

		if (lastPitOfTurn != gameBoard.length - 1
				&& lastPitOfTurn != ((gameBoard.length - 2) / 2)) {
			oppositePit = (gameBoard.length - 2) - lastPitOfTurn;
		}

		if (gameBoard[oppositePit] > 0 && lastPitOfTurn < gameBoard.length - 1
				&& lastPitOfTurn > ((gameBoard.length - 2) / 2) && this.theGame.getStones(lastPitOfTurn) == 0
				&& this.theGame.getStones(oppositePit) > 0) {
			gameBoard[this.theGame.getBoardSize() - 1] = this.theGame
					.getStones(this.theGame.getBoardSize() - 1) + this.theGame.getStones(oppositePit);
			this.theGame.setPitStones(this.theGame.getBoardSize() - 1, this.theGame
					.getStones(this.theGame.getBoardSize() - 1) + this.theGame.getStones(oppositePit));
			this.theGame.setPitStones(oppositePit, 0);
			this.theGame.setAreStonesCaptured(true);
		}

	}

}
