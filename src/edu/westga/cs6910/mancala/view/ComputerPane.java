package edu.westga.cs6910.mancala.view;

import edu.westga.cs6910.mancala.model.ComputerPlayer;
import edu.westga.cs6910.mancala.model.Game;
import edu.westga.cs6910.mancala.model.strategies.NearStrategy;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;

/**
 * Defines the pane that lets the user tell the computer player to take its turn
 * and that displays the setup of this player's side of the board
 * 
 * @author Chris Jones
 * @version June 11, 2019
 * 
 */
public class ComputerPane extends GridPane implements InvalidationListener {

	private Button btnTakeTurn;
	private ComputerPlayer theComputer;
	private Game theGame;

	/**
	 * Creates a new ComputerPane that observes the specified game.
	 * 
	 * @param theGame the model object from which this pane gets its data
	 * 
	 * @requires theGame != null
	 */
	public ComputerPane(Game theGame) {

		if (theGame == null) {
			throw new IllegalArgumentException("Invalid Game object");
		}

		this.theGame = theGame;
		this.theComputer = this.theGame.getComputerPlayer();
		this.theComputer.setStrategy(new NearStrategy());
		this.theGame.addListener(this);
		this.buildPane();

	}

	/**
	 * This method puts the ComputerPane together
	 */
	private void buildPane() {
		HBox topBox = new HBox();
		topBox.getChildren().add(new Label("Computer"));
		this.add(topBox, 0, 0);
		this.setHgap(50);
		this.add(new PitPane(this.theGame.getBoardSize() - 1, true, this.theGame), 0, 1);

		for (int column = this.theGame.getBoardSize() - 2; column > this.theGame.getBoardSize() / 2 - 1; column--) {
			this.add(new PitPane(column, false, this.theGame), this.theGame.getBoardSize() - column, 1);
		}

		this.createUserInteractionComputerSide();

	}

	/**
	 * This helper method creates the user interaction section of the ComputerPane
	 */
	private void createUserInteractionComputerSide() {
		GridPane interactionPane = new GridPane();
		interactionPane.getStyleClass().add("pane-border");
		interactionPane.getStyleClass().add("bg-highlight-style");

		this.btnTakeTurn = new Button("Take Turn");
		this.btnTakeTurn.setOnAction(new TakeTurnListener());
		interactionPane.add(this.btnTakeTurn, 0, 1);

		this.add(interactionPane, 5, 1);
	}

	/**
	 * This method shades the area whose turn it isn't.
	 */
	@Override
	public void invalidated(Observable arg0) {

		if (this.theGame.getIsGameOver()) {
			this.setDisable(true);
			return;
		}

		boolean myTurn = this.theGame.getCurrentPlayer() == this.theComputer;
		this.setDisable(!myTurn);
	}

	/*
	 * Defines the listener for takeTurnButton.
	 */
	private class TakeTurnListener implements EventHandler<ActionEvent> {

		/*
		 * Tells the Game to have its current player (i.e., the computer player) take
		 * its turn.
		 * 
		 * @see javafx.event.EventHandler#handle(T-extends-javafx.event.Event)
		 */
		@Override
		public void handle(ActionEvent arg0) {
			int pitChoice = ComputerPane.this.theGame.getComputerPlayer().getStrategy()
					.selectPit(ComputerPane.this.theGame.getGameBoard());

			if (!ComputerPane.this.theGame.getIsGameOver() && pitChoice != -1) {
				ComputerPane.this.theGame.play(pitChoice);
			}

			if (ComputerPane.this.theGame.getAnotherTurn()) {
				Alert anotherComputerTurn = new Alert(Alert.AlertType.INFORMATION);
				anotherComputerTurn.setTitle("Mancala Choice Issue");
				anotherComputerTurn.setHeaderText("The computer gets another turn.");
				anotherComputerTurn.showAndWait();
				
			}
		
			if (ComputerPane.this.theGame.getAreStonesCaptured()) {
				Alert captureStones = new Alert(Alert.AlertType.INFORMATION);
				captureStones.setTitle("Mancala Choice Issue");
				captureStones.setHeaderText("Your opponent captured some of your stones.");
				captureStones.showAndWait();
			}
		}
	}
}