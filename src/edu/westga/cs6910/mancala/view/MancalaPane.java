package edu.westga.cs6910.mancala.view;

import edu.westga.cs6910.mancala.model.Game;
import edu.westga.cs6910.mancala.model.Player;
import edu.westga.cs6910.mancala.model.strategies.FarStrategy;
import edu.westga.cs6910.mancala.model.strategies.NearStrategy;
import edu.westga.cs6910.mancala.model.strategies.RandomStrategy;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

/**
 * Defines a GUI for the Mancala game. This class was started by CS6910
 * 
 * @author Chris Jones
 * @version June 11, 2019
 * 
 */
public class MancalaPane extends BorderPane {
	private Game theGame;
	private GridPane pnContent;
	private HumanPane pnHumanPlayer;
	private ComputerPane pnComputerPlayer;
	private StatusPane pnGameInfo;
	private Pane pnChooseFirstPlayer;

	/**
	 * Creates a pane object to provide the view for the specified Game model
	 * object.
	 * 
	 * @param theGame the domain model object representing the Mancala game
	 * 
	 * @requires theGame != null
	 * @ensures the pane is displayed properly
	 */
	public MancalaPane(Game theGame) {

		if (theGame == null) {
			throw new IllegalArgumentException("Invalid Game object");
		}

		this.theGame = theGame;

		this.pnContent = new GridPane();

		this.addFirstPlayerChooserPane(theGame);

		this.addComputerPlayerPane(theGame);
		this.addHumanPane(theGame);
		this.addStatusPane(theGame);
		this.setCenter(this.pnContent);
		this.addMenus();
	}

	/**
	 * This method adds the menus
	 */
	private void addMenus() {
		MenuBar menuBar = new MenuBar();
		Menu fileMenu = new Menu("_F");
		fileMenu.setText("File");
		MenuItem exitMenu = new MenuItem("_X");
		
		exitMenu.setText("Exit");
		exitMenu.setAccelerator(KeyCombination.keyCombination("_F"));
		exitMenu.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				System.exit(0);
			}
		});
		
		MenuItem reStartMenu = this.addReStartMenu();
		fileMenu.getItems().add(exitMenu);
		fileMenu.getItems().add(reStartMenu);
		Menu settingsMenu = this.addSettingsMenu();
		MenuItem farOption = this.addFarOption();
		MenuItem nearOption = this.addNearOption();
		MenuItem randomOption = this.addRandomOption();
		Menu computerPlayerMenu = this.addcomputerPlayerMenu();
		computerPlayerMenu.getItems().addAll(farOption, nearOption, randomOption);
		menuBar.getMenus().addAll(fileMenu, settingsMenu);
		settingsMenu.getItems().add(computerPlayerMenu);
		HBox menuHBox = new HBox();
		menuHBox.getChildren().add(menuBar);
		this.pnContent.add(menuHBox, 0, 0);
	}
	
	/**
	 * This menu adds the restart menu item
	 * @return restart menu item
	 */
	private MenuItem addReStartMenu() {
		MenuItem reStartMenu = new MenuItem("_R");
		reStartMenu.setText("Restart");
		reStartMenu.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				MancalaPane.this.theGame.reStartGame();
				if (MancalaPane.this.theGame.getFirstPlayer() ==  MancalaPane.this.theGame.getHumanPlayer()) {
					MancalaPane.this.pnHumanPlayer.setDisable(false);
					MancalaPane.this.pnComputerPlayer.setDisable(true);	
					MancalaPane.this.theGame.startNewGame(MancalaPane.this.theGame.getHumanPlayer());
					MancalaPane.this.theGame.getHumanPlayer().setIsMyTurn(true);
					
				} else if (MancalaPane.this.theGame.getFirstPlayer() == MancalaPane.this.theGame.getComputerPlayer()) {
					MancalaPane.this.pnHumanPlayer.setDisable(true);
					MancalaPane.this.pnComputerPlayer.setDisable(false);
					MancalaPane.this.theGame.startNewGame(MancalaPane.this.theGame.getComputerPlayer());
					MancalaPane.this.theGame.getComputerPlayer().setIsMyTurn(true);	
				}
			}
		});
		return reStartMenu;
	}
	

	/**
	 * This method adds the settings menu
	 * @return settings menu
	 */
	private Menu addSettingsMenu() {
		Menu settingsMenu = new Menu("_S");
		settingsMenu.setText("Settings");
		return settingsMenu;
	}
	
	/**
	 * This method adds the computer play menu
	 * @return menu of computer play options
	 */
	private Menu addcomputerPlayerMenu() {
		Menu computerPlayerMenu = new Menu("_P");
		computerPlayerMenu.setText("Computer Player");
		return computerPlayerMenu;
	}

	/**
	 * This method returns the far option
	 * @return far option
	 */
	private MenuItem addFarOption() {
		MenuItem farOption = new MenuItem("_A");
		farOption.setText("Far");
		farOption.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				FarStrategy chosenFarStrategy = new FarStrategy();
				MancalaPane.this.theGame.getComputerPlayer().setStrategy(chosenFarStrategy);
			}
		});
		return farOption;
	}

	/**
	 * This method returns the near option
	 * @return near option
	 */
	private MenuItem addNearOption() {
		
		MenuItem nearOption = new MenuItem("_N");
		nearOption.setText("Near");
		nearOption.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				NearStrategy chosenNearStrategy = new NearStrategy();
				MancalaPane.this.theGame.getComputerPlayer().setStrategy(chosenNearStrategy);
			}
		});
		return nearOption;
	}

	/**
	 * This method returns the random option
	 * @return random option menu item
	 */
	private MenuItem addRandomOption() {
		MenuItem randomOption = new MenuItem("_R");
		randomOption.setText("Random");
		randomOption.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				RandomStrategy chosenRandomStrategy = new RandomStrategy();
				MancalaPane.this.theGame.getComputerPlayer().setStrategy(chosenRandomStrategy);
			}
		});
		return randomOption;
	}

	/**
	 * This method adds a ComputerPane
	 * 
	 * @param theGame game being played
	 */
	private void addComputerPlayerPane(Game theGame) {
		HBox upperMiddleBox = new HBox();
		upperMiddleBox.getStyleClass().add("pane-border");
		this.pnComputerPlayer = new ComputerPane(theGame);
		this.pnComputerPlayer.setDisable(true);
		upperMiddleBox.getChildren().add(this.pnComputerPlayer);
		this.pnContent.add(upperMiddleBox, 0, 2);
	}

	/**
	 * This method adds a HumanPane
	 * 
	 * @param theGame game being played
	 */
	private void addHumanPane(Game theGame) {
		HBox lowerMiddleBox = new HBox();
		lowerMiddleBox.getStyleClass().add("pane-border");
		this.pnHumanPlayer = new HumanPane(theGame);
		this.pnHumanPlayer.setDisable(true);
		lowerMiddleBox.getChildren().add(this.pnHumanPlayer);
		this.pnContent.add(lowerMiddleBox, 0, 3);
	}

	/**
	 * This method adds a FirstPlayerChooserPane
	 * 
	 * @param theGame game being played
	 */
	private void addFirstPlayerChooserPane(Game theGame) {
		HBox topBox = new HBox();
		topBox.getStyleClass().add("pane-border");
		this.pnChooseFirstPlayer = new NewGamePane(theGame);
		topBox.getChildren().add(this.pnChooseFirstPlayer);
		this.pnContent.add(topBox, 0, 1);
	}

	/**
	 * This method adds a StatusPane
	 * 
	 * @param theGame game being played
	 */
	private void addStatusPane(Game theGame) {
		HBox bottomScoreBox = new HBox();
		bottomScoreBox.getStyleClass().add("pane-border");
		this.pnGameInfo = new StatusPane(theGame);
		bottomScoreBox.getChildren().add(this.pnGameInfo);
		this.pnContent.add(bottomScoreBox, 0, 4);
	}

	/*
	 * Defines the panel in which the user selects which Player plays first.
	 */
	private final class NewGamePane extends GridPane {
		private RadioButton radHumanPlayer;
		private RadioButton radComputerPlayer;

		private Game theGame;
		private Player theHuman;
		private Player theComputer;

		private NewGamePane(Game theGame) {
			this.theGame = theGame;

			this.theHuman = this.theGame.getHumanPlayer();
			this.theComputer = this.theGame.getComputerPlayer();

			this.buildPane();
		}

		private void buildPane() {
			this.setHgap(10);
			this.radHumanPlayer = new RadioButton(this.theHuman.getName() + " first");
			this.radHumanPlayer.setOnAction(new HumanFirstListener());
			this.radComputerPlayer = new RadioButton(this.theComputer.getName() + " first");
			this.radComputerPlayer.setOnAction(new ComputerFirstListener());
			ToggleGroup buttonToggleGroup = new ToggleGroup();
			this.radHumanPlayer.setToggleGroup(buttonToggleGroup);
			this.radComputerPlayer.setToggleGroup(buttonToggleGroup);
			this.add(this.radHumanPlayer, 0, 0);
			this.add(this.radComputerPlayer, 1, 0);
		}

		/*
		 * Defines the listener for computer player first button.
		 */
		private class ComputerFirstListener implements EventHandler<ActionEvent> {
			@Override
			/**
			 * Enables the ComputerPlayerPane and starts a new game. Event handler for a
			 * click in the computerPlayerButton.
			 */
			public void handle(ActionEvent arg0) {
				MancalaPane.this.pnComputerPlayer.setDisable(false);
				MancalaPane.this.theGame.setFirstPlayer(MancalaPane.this.theGame.getComputerPlayer());
				MancalaPane.this.pnChooseFirstPlayer.setDisable(true);
				MancalaPane.this.theGame.startNewGame(NewGamePane.this.theComputer);
				MancalaPane.this.theGame.getComputerPlayer().setIsMyTurn(true);
			}
		}

		/*
		 * Defines the listener for human player first button.
		 */
		private class HumanFirstListener implements EventHandler<ActionEvent> {
			/*
			 * Sets up user interface and starts a new game. Event handler for a click in
			 * the human player button.
			 */
			@Override
			public void handle(ActionEvent event) {
				MancalaPane.this.pnChooseFirstPlayer.setDisable(true);
				MancalaPane.this.pnHumanPlayer.setDisable(false);
				MancalaPane.this.theGame.setFirstPlayer(MancalaPane.this.theGame.getHumanPlayer());
				MancalaPane.this.theGame.startNewGame(NewGamePane.this.theHuman);
				MancalaPane.this.theGame.getHumanPlayer().setIsMyTurn(true);
			}
		}
	}
}
