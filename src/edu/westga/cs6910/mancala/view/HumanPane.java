package edu.westga.cs6910.mancala.view;

import edu.westga.cs6910.mancala.model.Game;
import edu.westga.cs6910.mancala.model.HumanPlayer;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;

/**
 * Defines the panel that displays the setup of this player's side of the board.
 * It also allows the user to select the Pit Number to take the stones on their
 * turn.
 * 
 * @author CS6910
 * @version Summer 2019
 * @author Chris Jones
 * @version June 11, 2019
 */
public class HumanPane extends GridPane implements InvalidationListener {
	private ComboBox<String> cmbPitChoice;
	private Button btnTakeTurn;

	private HumanPlayer theHuman;
	private Game theGame;

	/**
	 * Creates a new HumanPane
	 * 
	 * @param theGame the model object from which this panel gets its data
	 * 
	 * @requires theGame != null
	 */
	public HumanPane(Game theGame) {
		if (theGame == null) {
			throw new IllegalArgumentException("Invalid Game object");
		}

		this.theGame = theGame;
		this.theGame.addListener(this);
		this.theHuman = this.theGame.getHumanPlayer();
		
		this.buildPane();
	}

	/**
	 * This method puts the HumanPane together
	 */
	private void buildPane() {
		HBox topBox = new HBox();
		topBox.getChildren().add(new Label("Human"));
		this.add(topBox, 0, 0);

		this.setHgap(50);

		this.createUserInteractionArea();

		int column = 0;
		for (column = 0; column < this.theGame.getBoardSize() / 2 - 1; column++) {
			this.add(new PitPane(column, false, this.theGame), column + 1, 1);
		}

		this.add(new PitPane(column, true, this.theGame), column + 1, 1);
	}

	private void createUserInteractionArea() {
		GridPane interactionPane = new GridPane();
		interactionPane.getStyleClass().add("pane-border");
		interactionPane.getStyleClass().add("bg-highlight-style");

		this.cmbPitChoice = new ComboBox<String>();
		this.cmbPitChoice.getItems().add("Please select");

		for (int count = 0; count < HumanPane.this.theGame.getBoardSize() / 2 - 1; count++) {
			this.cmbPitChoice.getItems().add("Pit " + count);
		}
		this.cmbPitChoice.getSelectionModel().select("Please select");
		interactionPane.add(this.cmbPitChoice, 0, 0);

		this.btnTakeTurn = new Button("Take Turn");
		this.btnTakeTurn.setOnAction(new TakeTurnListener());
		interactionPane.add(this.btnTakeTurn, 0, 1);

		this.add(interactionPane, 0, 1);
	}

	@Override
	public void invalidated(Observable arg0) {
		if (this.theGame.getIsGameOver()) {
			this.setDisable(true);
			return;
		}

		boolean myTurn = this.theGame.getCurrentPlayer() == this.theHuman;
		this.setDisable(!myTurn);
	}

	/**
	 * This class tells the game its current player.
	 * @author Chris Jones
	 * @version June 11, 2019
	 *
	 */
	public class TakeTurnListener implements EventHandler<ActionEvent> {
		/*
		 * Tells the Game to have its current player (i.e., the human Player) take its
		 * turn.
		 * 
		 */
		@Override
		public void handle(ActionEvent event) {
			int pitChoice = this.getSelectedPitNumber();
			int lastPitOfTurn = 0;
			if (pitChoice != -1) {
				lastPitOfTurn = HumanPane.this.theGame.getGameBoard()[pitChoice] + pitChoice;
			}
			if (!HumanPane.this.theGame.getIsGameOver() && pitChoice != -1) {
				HumanPane.this.theGame.play(pitChoice);	
			}
			
			if (lastPitOfTurn == ((HumanPane.this.theGame.getBoardSize() - 2) / 2) && pitChoice != -1
					&& !HumanPane.this.theGame.getIsGameOver()) {
				Alert anotherComputerTurn = new Alert(Alert.AlertType.INFORMATION);
				anotherComputerTurn.setTitle("Mancala Choice Issue");
				anotherComputerTurn.setHeaderText("You get another turn.");
				anotherComputerTurn.showAndWait();
			}
			
			if (HumanPane.this.theGame.getAreStonesCaptured()) {
				Alert captureStones = new Alert(Alert.AlertType.INFORMATION);
				captureStones.setTitle("Mancala Choice Issue");
				captureStones.setHeaderText("You captured your opponent's stones.");
				captureStones.showAndWait();
			}
		}

		/**
		 * Gets the pit selected by the user. If the selection is invalid, this method
		 * will display an Alert describing what happened
		 * 
		 * @return the selected pit number, if valid -1, otherwise
		 */
		private int getSelectedPitNumber() {
			if (HumanPane.this.cmbPitChoice.getValue().equals("Please select")) {
				Alert noStonesInPit = new Alert(Alert.AlertType.ERROR);
				noStonesInPit.setTitle("Mancala Choice Issue");
				noStonesInPit.setHeaderText("You must make a choice first.");
				noStonesInPit.showAndWait();
				return -1;
			} 

			int humanPlayerChoice = Integer.parseInt(HumanPane.this.cmbPitChoice.getValue().replace("Pit ", ""));
			if (HumanPane.this.theGame.getStones(humanPlayerChoice) <= 0) {
				Alert noStonesInPit = new Alert(Alert.AlertType.ERROR);
				noStonesInPit.setTitle("Mancala Choice Issue");
				noStonesInPit.setHeaderText("There are no stones in this pit.");
				HumanPane.this.theGame.setAreStonesCaptured(false);
				noStonesInPit.showAndWait();
				return -1;
			}
			
			return humanPlayerChoice;
		}
	}
	
}
