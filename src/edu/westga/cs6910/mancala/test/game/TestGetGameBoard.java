package edu.westga.cs6910.mancala.test.game;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.mancala.model.Game;

/**
 * This class tests the getGameBoard method in the Game class
 * @author Chris Jones
 * @version June 17, 2019
 *
 */
class TestGetGameBoard {
	
	/**
	 * This method tests the getGameBoard method
	 */
	@Test
	public void testGameBoardWhenNewGameIsCreated() {
		Game newGame = new Game();
		int[] mockBoard = new int[8];
		assertEquals(mockBoard.getClass(), newGame.getGameBoard().getClass());
	}

}
