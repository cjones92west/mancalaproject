package edu.westga.cs6910.mancala.test.game;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import edu.westga.cs6910.mancala.model.Game;
import edu.westga.cs6910.mancala.model.HumanPlayer;
import edu.westga.cs6910.mancala.model.strategies.NearStrategy;

/**
 * This class tests the getIsGameOver method
 * @author Chris Jones
 * @version June 17, 2019
 *
 */
class TestGetIsGameOver {

	/**
	 * This method tests whether a game is set to not being over after the creation of a new game.
	 */
	@Test
	public void  testBeforeGameStartsIsNotOver() {
		Game testGame = new Game();
		assertEquals(false, testGame.getIsGameOver());
			
	}
	
	/**
	 * This method tests whether a game is set to not being over after a turn is taken
	 */
	@Test
	public void  testWhileGameIsGoingOnButNotOver() {
		Game testGame = new Game();
		HumanPlayer newHuman = new HumanPlayer("Human", testGame);
		testGame.startNewGame(newHuman);
		assertEquals(false, testGame.getIsGameOver());
			
	}
	
	/**
	 * This method tests whether the getIsGameOver method returns true after a game has ended
	 */
	@Test
	public void  testWhenGameIsOverWithComputerWin() {
		Game testGame = new Game();
		NearStrategy newNearStrategy = new NearStrategy();
		testGame.getComputerPlayer().setStrategy(newNearStrategy);		
		testGame.startNewGame(testGame.getComputerPlayer());		
		testGame.play(6);
		testGame.play(5);
		testGame.play(2);
		testGame.play(1);
		testGame.play(4);
		
		assertEquals(true, testGame.getIsGameOver());
			
	}

}
