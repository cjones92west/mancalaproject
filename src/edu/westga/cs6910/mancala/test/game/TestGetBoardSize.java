package edu.westga.cs6910.mancala.test.game;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.mancala.model.Game;

/**
 * This class tests the getBoardSize method to ensure that the right size board
 * is returned.
 * 
 * @author Chris Jones
 * @version June 17, 2019
 *
 */
class TestGetBoardSize {

	/**
	 * This method tests that the method getBoardSize returns a board with the
	 * correct number when a new game is created
	 */
	@Test
	public void testNewGameShouldHaveBoardSize8() {
		Game newGame = new Game();
		assertEquals(8, newGame.getBoardSize());
	}

}
