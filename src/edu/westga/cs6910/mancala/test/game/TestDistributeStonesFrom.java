package edu.westga.cs6910.mancala.test.game;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.mancala.model.Game;
import edu.westga.cs6910.mancala.model.HumanPlayer;

/**
 * This class tests the distributeStonesFrom method
 * @author Chris Jones
 * @version June 19, 2019
 *
 */
class TestDistributeStonesFrom {

	/**
	 * This method tests if a pit gets emptied appropriately
	 */
	@Test
	public void testWhenStonesFirstDistributed() {
		Game newGame = new Game();
		HumanPlayer newHuman = new HumanPlayer("Human", newGame);
		newGame.startNewGame(newHuman);
		newGame.distributeStonesFrom(1);
		assertEquals(0, newGame.getStones(1));
			
	}
	
	/**
	 * This method tests if a pit gets filled up appropriately when stones are distributed twice
	 */
	@Test
	public void testWhenStonesDistributedTwice() {
		Game newGame = new Game();
		HumanPlayer newHuman = new HumanPlayer("Human", newGame);
		newGame.startNewGame(newHuman);
		newGame.distributeStonesFrom(0);
		newGame.distributeStonesFrom(1);
		assertEquals(2, newGame.getStones(2));
		
	}

}
