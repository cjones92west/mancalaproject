package edu.westga.cs6910.mancala.test.game;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.mancala.model.Game;
import edu.westga.cs6910.mancala.model.strategies.NearStrategy;

/**
 * This class tests the getTheWinner method
 * @author Chris Jones
 * @version July 2, 2019
 *
 */
class TestGetTheWinner {

	/**
	 * This method tests the getTheWinner method
	 */
	@Test
	public void testWhenGetTheWinner() {
		Game testGame = new Game();
		NearStrategy newNearStrategy = new NearStrategy();
		testGame.getComputerPlayer().setStrategy(newNearStrategy);		
		testGame.startNewGame(testGame.getComputerPlayer());		
		testGame.play(6);
		testGame.play(5);
		testGame.play(2);
		testGame.play(1);
		testGame.play(4);
		assertEquals(testGame.getComputerPlayer(), testGame.getTheWinner());
		
	}

}
