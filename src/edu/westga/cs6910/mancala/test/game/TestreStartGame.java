package edu.westga.cs6910.mancala.test.game;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.mancala.model.Game;

/**
 * This class tests the reStartGame method and its helper methods to ensure that it restarts the game correctly
 * @author Chris Jones
 * @version July 2, 2019
 *
 */
class TestreStartGame {

	/**
	 * This method tests whether the bank is successfully cleared when a new game starts
	 */
	@Test
	public void testBanksClearedWhenRestartGame() {
		Game newGame = new Game();
		newGame.startNewGame(newGame.getHumanPlayer());
		newGame.getHumanPlayer().takeTurn(2);
		newGame.reStartGame();
		assertEquals(0, newGame.getStones(3));
	}
	
	/**
	 * This method tests whether a sample pit is successfully cleared when a new game starts
	 */
	@Test
	void testPitsClearedWhenRestartGame() {
		Game newGame = new Game();
		newGame.startNewGame(newGame.getHumanPlayer());
		newGame.getHumanPlayer().takeTurn(1);
		newGame.reStartGame();
		assertEquals(1, newGame.getStones(2));
	}

}
