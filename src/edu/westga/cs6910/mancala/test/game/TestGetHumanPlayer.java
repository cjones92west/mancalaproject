package edu.westga.cs6910.mancala.test.game;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.mancala.model.Game;
import edu.westga.cs6910.mancala.model.HumanPlayer;

/**
 * This class tests the getHumanPlayer method in the Game class
 * @author Chris Jones
 * @version June 17, 2019
 *
 */
class TestGetHumanPlayer {

	/**
	 * This method tests the getHumanPlayer method
	 */
	@Test
	public void testHumanPlayerRetrievedAfterNewGameStarted() {
		Game newGame = new Game();
		HumanPlayer newHuman = new HumanPlayer("Human", newGame);
		assertEquals(true, newGame.getHumanPlayer().getClass().equals(newHuman.getClass()));
		
	}

}
