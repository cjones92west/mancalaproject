package edu.westga.cs6910.mancala.test.game;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.mancala.model.Game;
import edu.westga.cs6910.mancala.model.HumanPlayer;

/**
 * This class tests the getStones method
 * @author Chris Jones
 * @version June 18, 2019
 *
 */
class TestGetAndSetStones {

	/**
	 * This method tests the getStones method.
	 */
	@Test
	public void testWhenGameHasNotStartedForNoStones() {
		Game newGame = new Game();
		assertEquals(0, newGame.getStones(1));
		
	}
	
	/**
	 * This method tests the getStones method after a turn has been taken.
	 */
	@Test
	public void testWhenGameHasNotStartedForTwoStonesAfterFirstTurn() {
		Game newGame = new Game();
		HumanPlayer newHuman = new HumanPlayer("Human", newGame);
		newGame.startNewGame(newHuman);
		newGame.play(1);
		assertEquals(2, newGame.getStones(2));
		
	}
	
	/**
	 * This method tests the setStones method.
	 */
	@Test
	public void testWhenSetPitStones() {
		Game newGame = new Game();
		HumanPlayer newHuman = new HumanPlayer("Human", newGame);
		newGame.startNewGame(newHuman);
		newGame.play(1);
		newGame.setPitStones(2, 0);
		assertEquals(0, newGame.getStones(2));
		
	}
	
	/**
	 * This method tests the setStones method.
	 */
	@Test
	public void testWhenSetPitStonesTwice() {
		Game newGame = new Game();
		HumanPlayer newHuman = new HumanPlayer("Human", newGame);
		newGame.startNewGame(newHuman);
		newGame.play(1);
		newGame.setPitStones(2, 0);
		newGame.setPitStones(2, 1);
		assertEquals(1, newGame.getStones(2));
		
	}

}
