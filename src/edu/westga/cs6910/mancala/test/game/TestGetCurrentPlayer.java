package edu.westga.cs6910.mancala.test.game;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.mancala.model.ComputerPlayer;
import edu.westga.cs6910.mancala.model.Game;
import edu.westga.cs6910.mancala.model.HumanPlayer;
import edu.westga.cs6910.mancala.model.strategies.FarStrategy;

/**
 * This class tests the getCurrentPlayer method
 * @author Chris Jones
 * @version June 18, 2019
 *
 */
class TestGetCurrentPlayer {

	/**
	 * This method tests the getCurrentPlayer method for when the first player is a human
	 */
	@Test
	public void testWhenFirstPlayerisHuman() {
		Game newGame = new Game();
		HumanPlayer newHuman = new HumanPlayer("Human", newGame);
		newGame.startNewGame(newHuman);
		assertEquals(newHuman, newGame.getCurrentPlayer());
	}
	
	/**
	 * This method tests the getCurrentPlayer method for when the first player is a computer
	 */
	@Test
	public void testWhenFirstPlayerisComputer() {
		Game newGame = new Game();
		ComputerPlayer newComputer = new ComputerPlayer(newGame);
		newGame.startNewGame(newComputer);
		assertEquals(newComputer, newGame.getCurrentPlayer());
	}
	
	/**
	 * This method tests the getCurrentPlayer method for returning the correct player after a turn has taken
	 * place. If also tests the private swapWhoseTurn method.
	 */
	@Test
	public void testWhenFirstPlayerisComputerAndTurnIsOver() {
		Game newGame = new Game();
		FarStrategy newStrategy = new FarStrategy();
		newGame.getComputerPlayer().setStrategy(newStrategy);
		newGame.startNewGame(newGame.getComputerPlayer());
		newGame.play(1);
		assertEquals(newGame.getHumanPlayer(), newGame.getCurrentPlayer());
	}
	
}
