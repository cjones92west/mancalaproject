package edu.westga.cs6910.mancala.test.game;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import edu.westga.cs6910.mancala.model.Game;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;

/**
 * This class tests the removeListener method
 * 
 * @author Chris Jones
 * @version June 18, 2019
 *
 */
class TestRemoveListener {

	/**
	 * This class provides a mock object that implements InvalidationListener
	 * 
	 * @author Chris Jones
	 * @version June 18 ,2019
	 *
	 */
	public class Listener implements InvalidationListener {

		@Override
		public void invalidated(Observable arg0) {

		}

	}

	/**
	 * This class provides a place to remove the listener from the game
	 * 
	 * @author Chris Jones
	 * @version Juney 18, 2019
	 *
	 */
	public class TestListener {
		private boolean listenerIsAdded;
		private Game newGame;
		private Listener newListener;

		/**
		 * This constructor creates the TestListener object
		 * 
		 * @param newGame != null
		 */
		TestListener(Game newGame) {
			
			if (newGame == null) {
				throw new IllegalArgumentException("Invalid Game object");
			}
			this.listenerIsAdded = false;
			this.newListener = new Listener();
			this.newGame = newGame;

		}
		

		/**
		 * This method removes the listener from the game
		 * 
		 * @return boolean regarding whether the listener has been removed
		 */
		public boolean removeListenerFromGame() {
			this.newGame.removeListener(this.newListener);
			this.listenerIsAdded = false;
			return this.listenerIsAdded;
		}

		public boolean addListenerToGame() {
			this.newGame.addListener(this.newListener);
			this.listenerIsAdded = true;
			return this.listenerIsAdded;
		}
	}

	/**
	 * This method tests whether the listener has been removed
	 */
	@Test
	public void testWhenListenerIsRemoved() {
		Game newGame = new Game();
		TestListener newListener = new TestListener(newGame);
		newListener.addListenerToGame();
		newListener.removeListenerFromGame();

		assertEquals(false, newListener.removeListenerFromGame());

	}

}
