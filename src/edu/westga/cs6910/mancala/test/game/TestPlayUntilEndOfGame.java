package edu.westga.cs6910.mancala.test.game;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import edu.westga.cs6910.mancala.model.Game;
import edu.westga.cs6910.mancala.model.strategies.FarStrategy;
import edu.westga.cs6910.mancala.model.strategies.NearStrategy;


/**
 * This class tests the private methods involved in finishing the game and determining who the winner is
 * (getFinisher, finishGame).
 * @author Chris Jones
 * @version June 18, 2019
 *
 */
class TestPlayUntilEndOfGame {


	/**
	 * This method tests whether the right player wins after the game has ended
	 */
	@Test
	public void testWhenGameIsOverAndComputerShouldWin() {
		Game testGame = new Game();
		NearStrategy newNearStrategy = new NearStrategy();
		testGame.getComputerPlayer().setStrategy(newNearStrategy);		
		testGame.startNewGame(testGame.getComputerPlayer());		
		testGame.play(6);
		testGame.play(5);
		testGame.play(2);
		testGame.play(1);
		testGame.play(6);
		assertEquals(testGame.getComputerPlayer(), testGame.getTheWinner());
		assertEquals("Human: 2" + System.getProperty("line.separator") + "Simple computer: 4"
				+ System.getProperty("line.separator") + "Simple computer wins", testGame.toString());

	}

	/**
	 * This method tests whether the right player wins after a game
	 * has ended
	 */
	@Test
	public void testWhenGameIsOverWithHumanWin() {
		Game testGame = new Game();
		NearStrategy newNearStrategy = new NearStrategy();
		testGame.getComputerPlayer().setStrategy(newNearStrategy);
		testGame.startNewGame(testGame.getHumanPlayer());
		testGame.play(2);
		testGame.play(1);
		testGame.play(6);
		testGame.play(5);
		testGame.play(2);
		
		assertEquals("Human: 4" + System.getProperty("line.separator") + "Simple computer: 2"
				+ System.getProperty("line.separator") + "Human wins", testGame.toString());

	}
	
	
	
	/**
	 * This method tests whether the right player wins after a game
	 * has ended
	 */
	@Test
	public void testWhenGameIsOverWithATie() {
		Game testGame = new Game();
		FarStrategy newFarStrategy = new FarStrategy();
		testGame.getComputerPlayer().setStrategy(newFarStrategy);
		testGame.startNewGame(testGame.getHumanPlayer());
		testGame.play(2);
		testGame.play(1);
		testGame.play(5);
		testGame.play(2);
		testGame.play(0);
		testGame.play(2);
		
		assertEquals("Human: 3" + System.getProperty("line.separator") + "Simple computer: 3"
				 + "Tie game", testGame.toString());

	}

}
