package edu.westga.cs6910.mancala.test.game;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.mancala.model.ComputerPlayer;
import edu.westga.cs6910.mancala.model.Game;

/**
 * This class tests the getComputerPlayer method
 * @author Chris Jones
 * @version June 17, 2019
 *
 */
class TestGetComputerPlayer {

	/**
	 * This class tests the getHumanPlayer method in the Game class
	 * @author Chris Jones
	 * @version June 17, 2019
	 *
	 */
	class TestGetHumanPlayer {
		
		/**
		 * This method tests the getComputerPlayer method
		 */
		@Test
		public void testHumanPlayerRetrievedAfterNewGameStarted() {
			Game newGame = new Game();
			ComputerPlayer newComputer = new ComputerPlayer(newGame);
			assertEquals(true, newGame.getComputerPlayer().getClass().equals(newComputer.getClass()));
		}
	}
}
