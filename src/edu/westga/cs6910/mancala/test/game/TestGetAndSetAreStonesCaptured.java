package edu.westga.cs6910.mancala.test.game;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.mancala.model.Game;

/**
 * This class tests whether the getAreStonesCaptured method works
 * @author Chris Jones
 * @version July 2, 2019
 *
 */
class TestGetAndSetAreStonesCaptured {

	/**
	 * This method tests the getAreStonesCaptured method
	 */
	@Test
	public void testWhenStonesAreCaptured() {
		Game newGame = new Game();
		newGame.startNewGame(newGame.getHumanPlayer());
		newGame.play(2);
		newGame.play(1);
		assertEquals(true, newGame.getAreStonesCaptured());
		
	}
	
	/**
	 * This method tests the setAreStonesCaptured method
	 */
	@Test
	public void testWhenSetStonesAreCaptured() {
		Game newGame = new Game();
		newGame.startNewGame(newGame.getHumanPlayer());
		newGame.play(2);
		newGame.play(1);
		newGame.setAreStonesCaptured(false);
		assertEquals(false, newGame.getAreStonesCaptured());
		
	}

}
