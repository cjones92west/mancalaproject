package edu.westga.cs6910.mancala.test.game;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.mancala.model.Game;

/**
 * This class tests the getFirstPlayer and setFirstPlayer methods
 * @author Chris Jones
 * @version July 2, 2019
 *
 */
class TestGetAndSetFirstPlayer {

	/**
	 * This method tests the getFirstPlayer method to see if it returns the correct player
	 */
	@Test
	public void testGetFirstPlayerOfANewGame() {
		Game newGame = new Game();
		newGame.startNewGame(newGame.getComputerPlayer());
		assertEquals(newGame.getComputerPlayer(), newGame.getFirstPlayer());
	}
	
	/**
	 * This method tests the getFirstPlayer method to see if it returns the correct player
	 */
	@Test
	public void testWhenSetFirstPlayer() {
		Game newGame = new Game();
		newGame.startNewGame(newGame.getComputerPlayer());
		newGame.setFirstPlayer(newGame.getHumanPlayer());
		assertEquals(newGame.getHumanPlayer(), newGame.getFirstPlayer());
	}

}
