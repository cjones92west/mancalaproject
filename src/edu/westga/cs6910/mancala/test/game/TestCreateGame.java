package edu.westga.cs6910.mancala.test.game;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.mancala.model.Game;

/**
 * The class tests the Game class constructor to see if it starts a new game
 * correctly.
 * 
 * @author Chris Jones
 * @version June 17, 2019
 *
 */
class TestCreateGame {

	/**
	 * This method tests the Game class constructor and toString methods
	 */
	@Test
	public void testShouldProduceGameWithNoScore() {
		Game newGame = new Game();
		assertEquals("Human: 0" + System.getProperty("line.separator") + "Simple computer: 0", newGame.toString());
	}

}
