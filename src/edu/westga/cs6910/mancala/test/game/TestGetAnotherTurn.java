package edu.westga.cs6910.mancala.test.game;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.mancala.model.Game;
import edu.westga.cs6910.mancala.model.strategies.NearStrategy;

/**
 * This method tests the getAnotherTurn method to ensure that it is working
 * @author Chris Jones
 * @version July 5, 2019
 *
 */
class TestGetAnotherTurn {

	/**
	 * This method tests whether the player gets another turn when it is supposed to
	 */
	@Test
	public void testGetAnotherTurn() {
		Game newGame = new Game();
		newGame.startNewGame(newGame.getComputerPlayer());
		NearStrategy newNearStrategy = new NearStrategy();
		newGame.getComputerPlayer().setStrategy(newNearStrategy);
		newGame.play(6);
		assertEquals(true, newGame.getAnotherTurn());
		
	}

}
