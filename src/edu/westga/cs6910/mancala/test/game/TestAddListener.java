package edu.westga.cs6910.mancala.test.game;

import javafx.beans.InvalidationListener;
import javafx.beans.Observable;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.mancala.model.Game;



/**
 * This class tests the addListener method
 * 
 * @author Chris Jones
 * @version June 18, 2019
 *
 */
class TestAddListener {

	/**
	 * This class provides a mock object that implements InvalidationListener
	 * 
	 * @author Chris Jones
	 * @version June 18 ,2019
	 *
	 */
	public class Listener implements InvalidationListener {

		/**
		 * This is an implemented method.
		 */
		@Override
		public void invalidated(Observable arg0) {

		}

	}

	/**
	 * This class provides a place to add the listener to the game
	 * @author Chris Jones
	 * @version June 18, 2019
	 *
	 */
	public class TestListener {
		private boolean listenerIsAdded;
		private Game newGame;
		private Listener newListener;

		/**
		 * This constructs the object
		 * @param newGame
		 */
		TestListener(Game newGame) {
			if (newGame == null) {
				throw new IllegalArgumentException("Invalid Game object");
			}
			this.listenerIsAdded = false;
			this.newListener = new Listener();
			this.newGame = newGame;

		}

		/**
		 * This method adds the listener to the game
		 * @return boolean of whether listener was added
		 */
		public boolean addListenerToGame() {
			this.newGame.addListener(this.newListener);
			this.listenerIsAdded = true;
			return this.listenerIsAdded;
		}

	}

	/**
	 * This method tests that the listener is added
	 */
	@Test
	public void testWhenListenerIsAdded() {
		Game newGame = new Game();
		TestListener newListener = new TestListener(newGame);
		assertEquals(true, newListener.addListenerToGame());

	}

}
