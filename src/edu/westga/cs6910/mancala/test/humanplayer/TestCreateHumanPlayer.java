package edu.westga.cs6910.mancala.test.humanplayer;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.mancala.model.Game;
import edu.westga.cs6910.mancala.model.HumanPlayer;

/**
 * This class tests the constructor for HumanPlayer
 * 
 * @author Chris Jones
 * @version June 18, 2019
 *
 */
class TestCreateHumanPlayer {

	/**
	 * This method tests the constructor for HumanPlayer
	 */
	@Test
	public void testWhenCreateHumanPlayer() {
		Game newGame = new Game();
		HumanPlayer newHuman = new HumanPlayer("Human", newGame);
		assertEquals(newHuman.getClass(), newGame.getHumanPlayer().getClass());

	}

}
