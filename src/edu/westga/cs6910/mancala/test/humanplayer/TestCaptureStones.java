package edu.westga.cs6910.mancala.test.humanplayer;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.mancala.model.Game;

/**
 * This class tests the captureStones method as called when a capture happens during play
 * @author Chris Jones
 * @version July 7, 2019
 *
 */
class TestCaptureStones {

	/**
	 * This method tests that the bank of the human player has been correctly adjusted after a 
	 * capture has taken place
	 */
	@Test
	public void testForBankScoreAfterCaptureHappens() {
		Game newGame = new Game();
		newGame.startNewGame(newGame.getHumanPlayer());
		newGame.play(2);
		newGame.play(1);
		assertEquals(2, newGame.getStones(3));
	}
	
	/**
	 * This method tests that the opposite pit has been emptied after a capture has taken place
	 */
	@Test
	public void testForOppositePitScoreAfterCaptureHappens() {
		Game newGame = new Game();
		newGame.startNewGame(newGame.getHumanPlayer());
		newGame.play(2);
		newGame.play(1);
		assertEquals(0, newGame.getStones(4));
	}

}
