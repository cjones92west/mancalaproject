package edu.westga.cs6910.mancala.test.humanplayer;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.mancala.model.HumanPlayer;
import edu.westga.cs6910.mancala.model.strategies.NearStrategy;
import edu.westga.cs6910.mancala.model.Game;

/**
 * This class tests get and setIsMyTurn
 * @author Chris Jones
 * @version June 18, 2019
 *
 */
class TestSetAndGetIsMyTurn {

	/**
	 * This method tests getIsMyTurn when there is no game
	 */
	@Test
	public void testWhenThereIsNoGame() {
		Game newGame = new Game();
		HumanPlayer newGameHuman = newGame.getHumanPlayer();
		assertEquals(false, newGameHuman.getIsMyTurn());		
	}
	
	/**
	 * This method tests setIsMyTurn when there is no game
	 */
	@Test
	public void testSetIsMyTurn() {
		Game newGame = new Game();
		HumanPlayer newGameHuman = newGame.getHumanPlayer();
		newGameHuman.setIsMyTurn(true);
		assertEquals(true, newGameHuman.getIsMyTurn());		
	}
	
	/**
	 * This method tests getIsMyTurn when the human starts
	 */
	@Test
	public void testWhenThereIsAGameAndIsTheHumansTurn() {
		Game newGame = new Game();
		newGame.startNewGame(newGame.getHumanPlayer());
		assertEquals(true, newGame.getHumanPlayer().getIsMyTurn());		
	}
	
	/**
	 * This method tests getIsMyTurn when a few turns have gone by
	 */
	@Test
	public void testWhenAGameHasBeenGoingOnAndIsTheHumansTurn() {
		Game newGame = new Game();
		NearStrategy newNearStrategy = new NearStrategy();
		newGame.getComputerPlayer().setStrategy(newNearStrategy);
		newGame.startNewGame(newGame.getHumanPlayer());
		newGame.play(1);
		newGame.play(2);
		
		assertEquals(false, newGame.getHumanPlayer().getIsMyTurn());		
	}
}
