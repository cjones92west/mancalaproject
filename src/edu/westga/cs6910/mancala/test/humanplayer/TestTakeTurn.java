package edu.westga.cs6910.mancala.test.humanplayer;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.mancala.model.HumanPlayer;
import edu.westga.cs6910.mancala.model.Game;

/**
 * This class tests the takeTurn method
 * @author Chris Jones
 * @version June 19, 2019
 *
 */
class TestTakeTurn {

	/**
	 * This method tests the takeTurn method for when the Human goes first
	 */
	@Test
	public void testTakeFirstTurn() {
		Game newGame = new Game();
		HumanPlayer newHuman = newGame.getHumanPlayer();
		newGame.startNewGame(newHuman);
		newHuman.takeTurn(2);
		assertEquals(false, newHuman.getIsMyTurn());
	}
	
	/**
	 * This method tests the takeTurn method for when the Human goes first
	 */
	@Test
	public void testWhenDoNotGoFirst() {
		Game newGame = new Game();
		HumanPlayer newHuman = newGame.getHumanPlayer();
		newGame.startNewGame(newGame.getHumanPlayer());
		newGame.play(1);
		newHuman.takeTurn(2);
		assertEquals(false, newHuman.getIsMyTurn());
		
	}

}


