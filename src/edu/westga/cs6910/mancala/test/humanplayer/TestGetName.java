package edu.westga.cs6910.mancala.test.humanplayer;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.mancala.model.Game;
import edu.westga.cs6910.mancala.model.HumanPlayer;

/**
 * This class tests the getName method
 * @author Chris Jones
 * @version June 18, 2019
 *
 */
class TestGetName {

	/**
	 * This method tests the getName method
	 */
	@Test
	public void testWhenGetName() {
		Game newGame = new Game();
		HumanPlayer newHuman = newGame.getHumanPlayer();
		assertEquals("Human", newHuman.getName());
	}

}
