package edu.westga.cs6910.mancala.test.computerplayer;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.mancala.model.ComputerPlayer;
import edu.westga.cs6910.mancala.model.Game;

/**
 * This class tests the superclass method takeTurn for ComputerPlayer
 * @author Chris Jones
 * @version June 18, 2019
 *
 */
class TestTakeTurn {

	/**
	 * This method tests the takeTurn method for when the Computer goes first
	 */
	@Test
	public void testTakeFirstTurn() {
		Game newGame = new Game();
		ComputerPlayer newComputer = newGame.getComputerPlayer();
		newGame.startNewGame(newComputer);
		newComputer.takeTurn(2);
		assertEquals(false, newComputer.getIsMyTurn());
	}
	
	/**
	 * This method tests the takeTurn method for when the human goes first
	 */
	@Test
	public void testWhenDoNotGoFirst() {
		Game newGame = new Game();
		ComputerPlayer newComputer = newGame.getComputerPlayer();
		newGame.startNewGame(newGame.getHumanPlayer());
		newGame.play(1);
		newComputer.takeTurn(2);
		assertEquals(false, newComputer.getIsMyTurn());
		
	}

}
