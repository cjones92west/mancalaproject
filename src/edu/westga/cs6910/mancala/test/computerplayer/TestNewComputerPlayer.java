package edu.westga.cs6910.mancala.test.computerplayer;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.mancala.model.ComputerPlayer;
import edu.westga.cs6910.mancala.model.Game;

/**
 * This class tests that the ComputerPlayer constructor returns a ComputerPlayer object
 * @author Chris Jones
 * @version June 18, 2019
 *
 */
class TestNewComputerPlayer {

	/**
	 * This method tests the constructor
	 */
	@Test
	public void test() {
		Game newGame = new Game();
		ComputerPlayer testComputerPlayer = new ComputerPlayer(newGame);
		assertEquals(newGame.getComputerPlayer().getClass(), testComputerPlayer.getClass());
	}

}
