package edu.westga.cs6910.mancala.test.computerplayer;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.mancala.model.Game;
import edu.westga.cs6910.mancala.model.strategies.NearStrategy;

/**
 * This class tests the captureStones method as it is used in ComputerPlayer
 * @author Chris Jones
 * @version July 7, 2019
 *
 */
class TestCaptureStones {

	/**
	 * This method tests that the bank of the human player has been correctly adjusted after a 
	 * capture has taken place
	 */
	@Test
	public void testForBankScoreAfterCaptureHappens() {
		Game newGame = new Game();
		NearStrategy newNearStrategy = new NearStrategy();
		newGame.getComputerPlayer().setStrategy(newNearStrategy);
		newGame.startNewGame(newGame.getComputerPlayer());
		newGame.play(6);
		newGame.play(5);
		assertEquals(2, newGame.getStones(7));
	}
	
	/**
	 * This method tests that the opposite pit has been emptied after a capture has taken place
	 */
	@Test
	public void testForOppositePitScoreAfterCaptureHappens() {
		Game newGame = new Game();
		NearStrategy newNearStrategy = new NearStrategy();
		newGame.getComputerPlayer().setStrategy(newNearStrategy);
		newGame.startNewGame(newGame.getComputerPlayer());
		newGame.play(6);
		newGame.play(5);
		assertEquals(0, newGame.getStones(0));
	}

}



