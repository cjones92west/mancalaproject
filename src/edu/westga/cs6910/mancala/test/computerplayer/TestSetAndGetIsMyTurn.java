package edu.westga.cs6910.mancala.test.computerplayer;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.mancala.model.ComputerPlayer;
import edu.westga.cs6910.mancala.model.Game;
import edu.westga.cs6910.mancala.model.strategies.NearStrategy;

/**
 * This class tests the Superclass methods set and getIsMyTurn for ComputerPlayer
 * @author Chris Jones
 * @version June 18, 2019
 *
 */
class TestSetAndGetIsMyTurn {

	/**
	 * This method tests getIsMyTurn when there is no game
	 */
	@Test
	public void testWhenThereIsNoGame() {
		Game newGame = new Game();
		ComputerPlayer newGameComputer = newGame.getComputerPlayer();
		assertEquals(false, newGameComputer.getIsMyTurn());		
	}
	
	/**
	 * This method tests setIsMyTurn when there is no game
	 */
	@Test
	public void testSetIsMyTurn() {
		Game newGame = new Game();
		ComputerPlayer newGameComputer = newGame.getComputerPlayer();
		newGameComputer.setIsMyTurn(true);
		assertEquals(true, newGameComputer.getIsMyTurn());		
	}
	
	/**
	 * This method tests getIsMyTurn when a game is started with the computer
	 */
	@Test
	public void testWhenThereIsAGameAndIsTheComputersTurn() {
		Game newGame = new Game();
		newGame.startNewGame(newGame.getComputerPlayer());
		assertEquals(true, newGame.getComputerPlayer().getIsMyTurn());		
	}
	
	/**
	 * This method tests getIsMyTurn when it is the computer's turn during the game
	 */
	@Test
	public void testWhenAGameHasBeenGoingOnAndIsTheComputersTurn() {
		Game newGame = new Game();
		NearStrategy newStrategy = new NearStrategy();
		newGame.getComputerPlayer().setStrategy(newStrategy);
		newGame.startNewGame(newGame.getComputerPlayer());
		newGame.play(1);
		newGame.play(2);
		
		assertEquals(false, newGame.getComputerPlayer().getIsMyTurn());		
	}
}
