package edu.westga.cs6910.mancala.test.computerplayer;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.mancala.model.Game;
import edu.westga.cs6910.mancala.model.strategies.NearStrategy;

/**
 * This class tests the setStrategy method for the various strategies being employed
 * @author Chris Jones
 * @version June 23, 2019
 *
 */
class TestGetAndSetStrategy {

	/**
	 * This method tests the setStrategy when set to NearStrategy
	 */
	@Test
	public void testWhenUsingNearStrategy() {
		Game testGame = new Game();
		NearStrategy newNearStrategy = new NearStrategy();
		testGame.getComputerPlayer().setStrategy(newNearStrategy);
		testGame.startNewGame(testGame.getHumanPlayer());
		testGame.play(0);
		testGame.play(6);
		testGame.play(5);
		testGame.play(1);
		testGame.play(2);
		assertEquals("Human: 5" + System.getProperty("line.separator") + "Simple computer: 1"
				+ System.getProperty("line.separator") + "Human wins", testGame.toString());

	}
	
	/**
	 * This method tests the getStrategy method when set to NearStrategy
	 */
	@Test
	public void testGetStrategy() {
		Game testGame = new Game();
		NearStrategy newNearStrategy = new NearStrategy();
		testGame.getComputerPlayer().setStrategy(newNearStrategy);
		testGame.startNewGame(testGame.getHumanPlayer());
		assertEquals(newNearStrategy, testGame.getComputerPlayer().getStrategy());

	}
}
