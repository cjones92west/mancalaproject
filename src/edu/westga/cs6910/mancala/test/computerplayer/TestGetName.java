package edu.westga.cs6910.mancala.test.computerplayer;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.mancala.model.ComputerPlayer;
import edu.westga.cs6910.mancala.model.Game;

/**
 * This class tests whether the Superclass method getName works for ComputerPlayer
 * @author Chris Jones
 * @version June 18, 2019
 *
 */
class TestGetName {

	/**
	 * This method tests the getName method
	 */
	@Test
	public void testWhenGetName() {
		Game newGame = new Game();
		ComputerPlayer newComputer = newGame.getComputerPlayer();
		assertEquals("Simple computer", newComputer.getName());
	}

}
