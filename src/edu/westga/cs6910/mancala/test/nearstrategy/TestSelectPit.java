package edu.westga.cs6910.mancala.test.nearstrategy;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.mancala.model.Game;
import edu.westga.cs6910.mancala.model.strategies.NearStrategy;

/**
 * This class tests the implementation of the SelectStrategy
 * @author Chris Jones
 * @version July 2, 2019
 *
 */
class TestSelectPit {

	/**
	 * This method tests the implementation of the nearStrategy
	 */
	@Test
	public void testSelectPitForFarStrategy() {
		Game newGame = new Game();
		newGame.startNewGame(newGame.getComputerPlayer());
		NearStrategy newSelectStrategy = new NearStrategy();
		assertEquals(6, newSelectStrategy.selectPit(newGame.getGameBoard()));

	}
	
	/**
	 * This method tests the selectPit method for a FarStrategy choice
	 */
	@Test
	public void testSelectPitForFarStrategyIfInitialSelectionHasNoPits() {
		Game newGame = new Game();
		newGame.startNewGame(newGame.getComputerPlayer());
		NearStrategy newSelectStrategy = new NearStrategy();
		newGame.getComputerPlayer().setStrategy(newSelectStrategy);
		newGame.getComputerPlayer().takeTurn(newGame.getComputerPlayer().getStrategy().selectPit(newGame.getGameBoard()));

		assertEquals(5, newSelectStrategy.selectPit(newGame.getGameBoard()));

	}
}
