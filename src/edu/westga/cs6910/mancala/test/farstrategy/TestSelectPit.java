package edu.westga.cs6910.mancala.test.farstrategy;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import edu.westga.cs6910.mancala.model.Game;
import edu.westga.cs6910.mancala.model.strategies.FarStrategy;

/**
 * This class tests the FarStrategy's implementation of the strategy
 * 
 * @author Chris Jones
 * @version June 23, 2019
 *
 */
class TestSelectPit {

	/**
	 * This method tests the selectPit method for a FarStrategy choice
	 */
	@Test
	public void testSelectPitForFarStrategy() {
		Game newGame = new Game();
		newGame.startNewGame(newGame.getComputerPlayer());
		FarStrategy newSelectStrategy = new FarStrategy();
		assertEquals(4, newSelectStrategy.selectPit(newGame.getGameBoard()));

	}
	
	
	/**
	 * This method tests the selectPit method for a FarStrategy choice
	 */
	@Test
	public void testSelectPitForFarStrategyIfInitialSelectionHasNoPits() {
		Game newGame = new Game();
		newGame.startNewGame(newGame.getComputerPlayer());
		FarStrategy newSelectStrategy = new FarStrategy();
		newGame.getComputerPlayer().setStrategy(newSelectStrategy);
		newGame.getComputerPlayer().takeTurn(newGame.getComputerPlayer().getStrategy().selectPit(newGame.getGameBoard()));

		assertEquals(5, newSelectStrategy.selectPit(newGame.getGameBoard()));

	}

}
